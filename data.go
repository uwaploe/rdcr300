package main

import (
	"bytes"
	"context"
	"errors"
	"io"
	"log"
	"os"
	"strconv"
	"text/template"
	"time"

	nmea "bitbucket.org/mfkenney/go-nmea"
	"github.com/gomodule/redigo/redis"
)

type dataRecord struct {
	T time.Time
	D map[string]float64
}

// Parse an NMEA XDR sentence into a map with one entry for each "transducer".
func parseRecord(s nmea.Sentence) (map[string]float64, error) {
	rec := make(map[string]float64)
	for i := 0; i < len(s.Fields); i += 4 {
		val, err := strconv.ParseFloat(s.Fields[i+1], 64)
		if err != nil {
			return rec, err
		}
		rec[s.Fields[i+3]] = val
	}
	return rec, nil
}

// NMEA tag for the CR300 data records
const TAG = "CSXDR"

func streamData(ctx context.Context, r io.Reader) <-chan dataRecord {
	ch := make(chan dataRecord, 1)
	go func() {
		defer close(ch)
		for ctx.Err() == nil {
			select {
			case <-ctx.Done():
				return
			default:
			}

			s, err := nmea.ReadSentence(r)
			t_sample := time.Now().UTC()
			if err != nil {
				if !errors.Is(err, os.ErrDeadlineExceeded) {
					log.Printf("NMEA read error: %v", err)
				}
				continue
			}
			if s.Id != TAG {
				log.Printf("IGNORED: %q\n", s)
				continue
			}
			if rec, err := parseRecord(s); err == nil {
				ch <- dataRecord{T: t_sample, D: rec}
			} else {
				log.Printf("Invalid record: %q", s)
			}
		}
	}()

	return ch
}

func createMessage(t *template.Template, dr dataRecord) []byte {
	var buf bytes.Buffer
	t.Execute(&buf, dr)
	return buf.Bytes()
}

func publishMessage(conn redis.Conn, chName string, t *template.Template,
	dr dataRecord) {
	conn.Do("PUBLISH", chName, createMessage(t, dr))
}
