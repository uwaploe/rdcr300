# DOT CR300 Data Acquisition

This repository contains the source code for rdcr300, a program to log data records from the Campbell Scientific CR300 "met station". The DOT CR300 runs a custom CRBasic program which samples a barometer and temperature sensor along with several system battery voltages and outputs the data at 1Hz as an NMEA XDR sentence.

## Operation

Rdcr300 is run by a [Systemd Timer](https://www.blunix.com/blog/ultimate-tutorial-about-systemd-timers.html), the default schedule is once per minute. It acquires 10 samples and sends each one to the Redis channel *data.cr300* in CSV format. It writes the average data record to the channel *data.cr300avg*.

## Installation

Download the latest Debian package from the *Downloads* page of this repository and install with `dpkg`:

``` shellsession
$ dpkg -i rdcr300_VERSION_any.deb
```

If any prerequisite software is missing, the `dpkg` command will fail. If this occurs, run `apt-get` to install the missing software:

``` shellsession
$ apt-get --yes install -f
```
