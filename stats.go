package main

import "math"

// Keep a running average of a map of floating-point values
type accumulator struct {
	avg   map[string]float64
	count int
}

func newAccumulator() *accumulator {
	return &accumulator{
		avg: make(map[string]float64),
	}
}

// Add a new sample to the running average.
func (a *accumulator) addSample(rec map[string]float64) int {
	a.count++
	alpha := 1. / float64(a.count)
	beta := 1.0 - alpha
	for k, v := range rec {
		if math.IsNaN(v) || math.IsInf(v, 0) {
			continue
		}
		a.avg[k] = v*alpha + beta*a.avg[k]
	}
	return a.count
}

// Reset the accumulator
func (a *accumulator) reset() {
	a.count = 0
	a.avg = make(map[string]float64)
}

// Return a map containing the current average values
func (a *accumulator) getMean() map[string]float64 {
	return a.avg
}

func (a *accumulator) len() int {
	return a.count
}
