module bitbucket.org/uwaploe/rdcr300

go 1.22

toolchain go1.22.4

require (
	bitbucket.org/mfkenney/go-nmea v1.6.1
	github.com/gomodule/redigo v1.8.5
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	periph.io/x/conn/v3 v3.7.1
	periph.io/x/host/v3 v3.8.2
)

require golang.org/x/sys v0.0.0-20181210030007-2a47403f2ae5 // indirect
