package main

import (
	"reflect"
	"testing"
	"text/template"
	"time"

	nmea "bitbucket.org/mfkenney/go-nmea"
)

func TestRecParse(t *testing.T) {
	s, _ := nmea.ParseSentence([]byte("$CSXDR,C,19.0,C,TempAir,P,1020.8745,B,Barometer,V,12.2,V,VBatt"))
	rec, err := parseRecord(s)
	if err != nil {
		t.Fatal(err)
	}

	table := []struct {
		name  string
		value float64
	}{
		{name: "TempAir", value: 19},
		{name: "Barometer", value: 1020.8745},
		{name: "VBatt", value: 12.2},
	}

	for _, e := range table {
		if rec[e.name] != e.value {
			t.Errorf("Bad value for %q; expected %v, got %v", e.name,
				e.value, rec[e.name])
		}
	}
}

func TestRecFormat(t *testing.T) {
	s, _ := nmea.ParseSentence([]byte("$CSXDR,C,19.0,C,TempAir,P,1020.8745,B,Barometer,V,12.2,V,VBatt,V,15.10,V,VSolar,V,15.20,V,VSolarPost,V,15.30,V,VAlkaline,H,42.1,%,RH"))
	rec, err := parseRecord(s)
	if err != nil {
		t.Fatal(err)
	}

	fm := template.FuncMap{
		"tfmt": formatTime,
	}
	tmpl, err := template.New("rec").Funcs(fm).Parse(FORMAT)
	if err != nil {
		t.Fatal(err)
	}

	ts := time.Date(2006, time.January, 2, 15, 4, 5, 0, time.UTC)
	msg := string(createMessage(tmpl, dataRecord{T: ts, D: rec}))
	expect := "2006-01-02 15:04:05,19.0,1020.87,12.2,15.10,15.20,15.30,42.1"
	if msg != expect {
		t.Errorf("Bad message; expected %q, got %q", expect, msg)
	}

}

func TestAccumulator(t *testing.T) {
	table := []struct {
		in   map[string]float64
		mean map[string]float64
	}{
		{
			in:   map[string]float64{"a": 1, "b": 2, "c": 3},
			mean: map[string]float64{"a": 1, "b": 2, "c": 3},
		},
		{
			in:   map[string]float64{"a": 1, "b": 1, "c": 1},
			mean: map[string]float64{"a": 1, "b": 1.5, "c": 2},
		},
		{
			in:   map[string]float64{"a": 1, "b": 3, "c": 5},
			mean: map[string]float64{"a": 1, "b": 2, "c": 3},
		},
	}

	acc := newAccumulator()
	for _, e := range table {
		acc.addSample(e.in)
		m := acc.getMean()
		if !reflect.DeepEqual(m, e.mean) {
			t.Errorf("Bad value; expected %#v, got %#v", e.mean, m)
		}
	}
}
