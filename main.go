// Rdcr300 reads data records from the Campbell Scientific CR300 data
// logger on the DOT Buoy and publishes them to a Redis pub-sub
// channel. The CR300 is running a custom data collection program which
// outputs sensor data as NMEA XDR sentences.
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"text/template"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/tarm/serial"
	"periph.io/x/conn/v3/gpio"
	"periph.io/x/conn/v3/gpio/gpioreg"
	host "periph.io/x/host/v3"
)

var Version = "dev"
var BuildDate = "unknown"

var Usage = `Usage: rdcr300 [options] serialdev N

Read N NMEA data records from a CR300 data logger attached to SERIALDEV and
publish the data values to a Redis pub-sub channel in CSV format. The N
sample average for each data value is published to a second channel.

`

// Default template for the output CSV records
const FORMAT = `{{.T|tfmt}},{{.D.TempAir|printf "%.1f"}},{{.D.Barometer|printf "%.2f"}},{{.D.VBatt|printf "%.2f"}},{{.D.VSolar|printf "%.2f"}},{{.D.Vaux|printf "%.2f"}},{{.D.Vpr|printf "%.2f"}},{{.D.TempInternal|printf "%.1f"}}`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	debug     bool
	dataFmt   string        = FORMAT
	rdAddr    string        = "localhost:6379"
	rdChan    string        = "data.cr300"
	avgChan   string        = "data.cr300avg"
	inBaud    int           = 115200
	gpioLine  int           = 117
	warmUp    time.Duration = time.Second * 5
	rdTimeout time.Duration = time.Second * 3
	noStop    bool
)

// Template formatting function
func formatTime(t time.Time) string {
	return t.UTC().Format("2006-01-02 15:04:05")
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.BoolVar(&debug, "debug", debug, "Output diagnostic information while running")
	flag.StringVar(&dataFmt, "fmt", dataFmt, "Output data format template")
	flag.StringVar(&rdAddr, "rdaddr", rdAddr, "Redis server HOST:PORT")
	flag.StringVar(&rdChan, "rdchan", rdChan, "Redis pub-sub channel")
	flag.StringVar(&avgChan, "avgchan", avgChan, "Redis pub-sub channel for averaged data")
	flag.IntVar(&inBaud, "baud", inBaud, "CR300 baud rate")
	flag.BoolVar(&noStop, "no-stop", noStop, "do not shutdown CR300 on exit")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()

	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	avgN, err := strconv.Atoi(args[1])
	if err != nil {
		log.Fatalf("Invalid sample count: %q", args[1])
	}

	_, err = host.Init()
	if err != nil {
		log.Fatalf("GPIO library: %v", err)
	}

	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	// GPIO line to switch power
	var sw gpio.PinOut
	if gpioLine > 0 {
		sw = gpioreg.ByName(fmt.Sprintf("GPIO%d", gpioLine))
		if sw == nil {
			log.Fatalf("Invalid GPIO line: %d", gpioLine)
		}
		sw.Out(gpio.Low)
	}

	fm := template.FuncMap{
		"tfmt": formatTime,
	}
	tmpl := template.Must(template.New("rec").Funcs(fm).Parse(dataFmt))

	cfg := &serial.Config{
		Name:        args[0],
		Baud:        inBaud,
		ReadTimeout: rdTimeout,
	}
	port, err := serial.OpenPort(cfg)
	if err != nil {
		log.Fatalf("Serial port open failed: %v", err)
	}

	var conn redis.Conn
	if rdChan != "" {
		conn, err = redis.Dial("tcp", rdAddr)
		if err != nil {
			log.Fatalf("Cannot connect to Redis: %v", err)
		}
		defer conn.Close()
	}

	ctx, stop := signal.NotifyContext(context.Background(),
		syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer stop()

	log.Printf("DOT Buoy CR300 data reader (%s)", Version)

	if sw != nil {
		// Power-on the CR300 and power-off on exit
		err = sw.Out(gpio.High)
		if err != nil {
			log.Fatalf("cannot power-on CR300: %v", err)
		}
		log.Println("CR300 powered on")
		time.Sleep(warmUp)
		if !noStop {
			defer func() {
				sw.Out(gpio.Low)
				log.Printf("CR300 powered off")
			}()
		}
	}

	if !noStop {
		defer func() {
			// Cleanly shutdown the CR300 firmware
			port.Write([]byte("\x1b\x1b"))
		}()
	}

	acc := newAccumulator()
	ch := streamData(ctx, port)

	for dr := range ch {
		if rdChan != "" {
			publishMessage(conn, rdChan, tmpl, dr)
		} else {
			fmt.Printf("%s\n", createMessage(tmpl, dr))
		}

		acc.addSample(dr.D)
		if acc.len() == avgN {
			mean := acc.getMean()
			acc.reset()
			if rdChan != "" && avgChan != "" {
				publishMessage(conn, avgChan, tmpl, dataRecord{T: dr.T, D: mean})
			} else {
				fmt.Printf("AVG:%s\n", createMessage(tmpl, dataRecord{T: dr.T, D: mean}))
			}
			// Stop streamData
			stop()
		}

	}

}
